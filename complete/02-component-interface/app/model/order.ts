export interface Address {
  company: string;
  contact: string;
  address: string;
  city: string;
  state: string;
}

export interface OrderedItem {
  productId: string,
  productName: string,
  quantity: number,
  pricePerPiece: number,
  totalPrice: number
}

export interface Order {
  guid: string;
  billingAddress: Address;
  shippingAddress?: Address;
  email: string;
  phone: string;
  note?: string;
  orderedItems: OrderedItem[],
  totalPrice: number
}
