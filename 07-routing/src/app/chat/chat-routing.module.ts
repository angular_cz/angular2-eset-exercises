import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    // TODO 2.1 - přidejte routu pro chat

    // TODO 2.4 - doplňte routu roooms
    // TODO 3.2 - přidejte resolver
    // TODO 2.5 - doplňte routu pro detail
  },
];

@NgModule({
  // TODO 2.2 - importujte routy
  imports: [],
  exports: [RouterModule],

  // TODO 3.2 - registrujte RoomsResolver
  providers: []
})
export class ChatRoutingModule {
}
