import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatRoom, Message } from "../model/chat";
import { Http, Response } from "@angular/http";

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/toPromise'
import { Observable, Subject, Subscription } from "rxjs";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {

  readonly CHATROOM_URL = 'http://localhost:8000/rooms/1';

  // TODO 1.1 - odstraňte statická data
  chatRoom: ChatRoom = {
    room: {
      id: 1,
      title: 'Angular 2 basics'
    },
    messages: [{
      user: 'Kayley Crona',
      text: 'Quia accusamus temporibus ullam consectetur porro exercitationem unde consequuntur. Consequuntur dolorem voluptatibus tempore quos. Repudiandae a voluptate non quod fugiat. Quis consequatur nihil doloremque animi consectetur debitis eligendi cum. Veritatis deserunt ratione deleniti cumque. Odit quidem reprehenderit. Nesciunt commodi et dolorem excepturi aperiam consequatur nisi vero ea. Veniam optio optio illum quo sed hic placeat qui repellat. Tempore ullam voluptatem corrupti aspernatur voluptates distinctio molestias porro.'
    }]
  };
  chatRoomSubscription$: Subscription;

  // TODO 2.2 - vytvořte nový observable subject
  myMessages$ = null;

  myMessagesStream$: Observable<Response>;

  constructor(private http: Http) {
    // TODO 2.3 - vytvořte obsluhu pro odesílání zprávy
    // TODO 3.1 - uložte obsluhu do streamu

  }

  ngOnInit(): void {

    // TODO 1.1 - načtěte chatRoom z url this.CHATROOM_URL

    // TODO 1.2 - načítejte chatRoom periodicky
    // TODO 3.2 - připojte k stream odesílání

  }

  sendMyMessage(message: Message) {
    // TODO 2.1 - odešlete zprávu pomocí http metody post
    // TODO 2.4 - posílejte zprávu do streamu

  }

  ngOnDestroy() {
    // TODO 3.3 - odregistrujte stream

  }
}
